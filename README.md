# OTR: Obey The Rules! #

A simple and raw tool to check installation sanity

### Why? ###

You know when you client call you because your software doesn't work, there are corruptions, missing libraries, etc.  
DevOps can't help because they think it's a software issue, but in many cases it's only a matter of installation.  
A bad installation, where something went wrong, some files were screwed, an environment variable was not defined correctly...


### What is OTR for? ###

OTR helps you to describe a correct installation for your software with one or more rules.

You tell OTR:

* where the home of your installed package is
* which files are supposed to be there
* if some environment variables should be defined


### Usage ###

To install dependencies:
```
pip install -r requirements.txt
```

To launch:
```
otr.py -f [rule-file(s)]
```


### Features ###
* TBD
