#!/usr/bin/env python

import yaml
import argparse
import os
from colorama import init, Fore, Back, Style
init(autoreset=True)


# highlight text
def highlight(text):
    return Back.YELLOW+Style.BRIGHT+text


def mark_ok(text):
    return Fore.GREEN+Style.BRIGHT+text+Fore.RESET+Style.NORMAL


# mark passed text as error (red)
def otr_error(text):
    print Fore.RED+Style.BRIGHT+"ERROR\t"+text


def otr_print(text, tag="INFO"):
    print tag+"\t"+text


# resolve passed environment variable and return true if exists
def check_env_var(var):
    os_var = os.environ.get(var)

    if os_var is None:
        return False
    else:
        return True


def check_path(path):
    return os.path.exists(path)


def check_file(home_dir, file_name):
    if check_path(os.path.join(home_dir, file_name)):
        return True
    else:
        return False


def otr_check_file(home_dir, file_name, print_errors_only):
    if check_file(home_dir, file_name):
        if not print_errors_only:
            otr_print("%s found" % mark_ok(file_name), "FILES")
    else:
        otr_error("File %s not found" % os.path.join(home_dir, file_name))


# print env var check
def otr_check_env_var(var, print_errors_only):
    if check_env_var(var):
        if not print_errors_only:
            otr_print("%s found" % mark_ok(var), "ENV")
    else:
        otr_error("Environment variable %s not found" % var)


####################################
# MAIN
####################################

parser = argparse.ArgumentParser(description='=== Obey The Rules ===')
parser.add_argument('-f', '--files', nargs='+', help='List of rule files',
                    required=True)
parser.add_argument('-e', '--errors_only',
                    help='Print errors only', action='store_true')

args = parser.parse_args()

# check every passed rule
for a_file in args.files:
    rule_file = yaml.load(file(a_file, 'r'))

    print ""
    otr_print("Loading rule %s@%s..." % (highlight(rule_file["name"]),
                                         rule_file["version"]))

    home_dir = ""

    # read home directory
    if rule_file["home_dir"]["type"] == "path":
        home_dir = rule_file["home_dir"]["text"]
        if check_path(home_dir) is False:
            otr_error("Home directory %s not found" % home_dir)
            continue
        else:
            otr_print("Current home dir is %s" % home_dir)
    else:
        home_dir = os.environ.get(rule_file["home_dir"]["text"])
        if home_dir is None:
            otr_error("Home directory %s not found" %
                      rule_file["home_dir"]["text"])
            continue
        else:
            if check_path(home_dir) is False:
                otr_error("Home directory %s not found" % home_dir)
                continue
            else:
                otr_print("Current home dir is %s" % highlight(home_dir))

    # read env vars from rule
    if "environment" in rule_file:
        env_vars = rule_file["environment"]

        for var in env_vars:
            otr_check_env_var(var, args.errors_only)

    # look for requested files
    if home_dir is not None:
        for searched_file in rule_file["files"]:
            otr_check_file(home_dir, searched_file, args.errors_only)
